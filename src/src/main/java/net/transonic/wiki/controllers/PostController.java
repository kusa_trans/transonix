package net.transonic.wiki.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import java.util.Optional;

import net.transonic.wiki.entities.Post;
import net.transonic.wiki.repositories.PostRepository;

@Controller
public class PostController {
    @Autowired
    PostRepository repository;

    @RequestMapping(path="/posts", method=RequestMethod.GET)
    public String index(Model model) {
        Iterable<Post> posts = repository.findAll();
        model.addAttribute("posts", posts);
        return "post/index";
    }

    @RequestMapping(path="/posts/new", method=RequestMethod.GET)
    public String create(Model model) {
        Post post = new Post();
        model.addAttribute("post", post);
        return "post/create";
    }

    @RequestMapping(path="/posts", method=RequestMethod.POST)
    @Transactional(readOnly=false)
    public String store(@ModelAttribute("formModel") Post post) {
        repository.saveAndFlush(post);
        return "redirect:/posts";
    }

    @RequestMapping(path="/posts/{id}", method=RequestMethod.GET)
    public String show(@ModelAttribute Post post, @PathVariable int id, Model model) {
        Optional<Post> data = repository.findById((long)id);
        model.addAttribute("post", data.get());
        return "post/show";
    }

    @RequestMapping(path="/posts/{id}/edit", method=RequestMethod.GET)
    public String edit(@ModelAttribute Post post, @PathVariable int id, Model model) {
        Optional<Post> data = repository.findById((long)id);
        model.addAttribute("post", data.get());
        return "post/edit";
    }

    @RequestMapping(path="/posts/{id}", method=RequestMethod.POST)
    @Transactional(readOnly=false)
    public String update(@ModelAttribute Post post) {
        repository.saveAndFlush(post);
        return "redirect:/posts";
    }

    @RequestMapping(path="/posts/{id}/delete", method=RequestMethod.POST)
    @Transactional(readOnly=false)
    public String destroy(@PathVariable int id) {
        repository.deleteById((long)id);
        return "redirect:/posts";
    }

}

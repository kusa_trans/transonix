package net.transonic.wiki.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long id;

    @Column(length=50, nullable=false)
    private String title;

    @Column(length=200, nullable=false)
    private String content;

    @Column(length=50, nullable=false)
    private String writer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public  void setContent(String content) {
        this.content = content;
    }

    public String getWriter() {
        return writer;
    }

    public  void setWriter(String writer) {
        this.writer = writer;
    }




}

package net.transonic.wiki.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.transonic.wiki.entities.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

}


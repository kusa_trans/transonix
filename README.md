# TransoniX

Spring Boot サンプルアプリ

## 開発環境構築

- Spring Boot
- HSQLDB (インメモリデータベース)
データベースにMySQLを使いたい場合は、Dockerでコンテナを用意すればよい。

Spring Tool Suiteという公式の統合開発環境があるので、そちらを使うとよい。
VSCodeでも拡張機能が提供されているので、簡単に開発できる。


1. サンプルをクローン

2. VSCodeの拡張機能「Spring Boot Extension Pack」を追加

3. 左メニュー「Spring Boot Dashboard」から開発サーバー起動
デバッグもできる


docker-composeは、jarファイルのコンパイルができる環境を設定済みだが、使わなくても開発できる。